<?php
namespace WPez\WPezTraits\Setters;

trait TraitSetArray {

    protected function setArray( $str_prop = false, $arr = false, $int_min_count = 0, $int_max_count = false ) {

        if ( property_exists( $this, $str_prop )
             && is_array( $arr )
             && count($arr) >= absint($int_min_count)
             && ( $int_max_count === false  || count($arr) <= absint($int_max_count) ) )  {

            $this->$str_prop = $arr;

            return true;
        }
        return false;
    }
}