<?php

namespace WPez\WPezTraits\Setters;

trait TraitAll {

    use \WPez\WPezTraits\Setters\TraitSetArray;
    use \WPez\WPezTraits\Setters\TraitSetBool;
    use \WPez\WPezTraits\Setters\TraitSetEmail;
    use \WPez\WPezTraits\Setters\TraitSetFloat;
    use \WPez\WPezTraits\Setters\TraitSetHex;
    use \WPez\WPezTraits\Setters\TraitSetInteger;
    use \WPez\WPezTraits\Setters\TraitSetIP;
    use \WPez\WPezTraits\Setters\TraitSetRegexp;
    use \WPez\WPezTraits\Setters\TraitSetString;
    use \WPez\WPezTraits\Setters\TraitSetURL;

}