## WPezTraits: Setters

__Collection of boilerplate setters with appropriate validations.__

Many rely on PHP's filter_var(). Ref: http://php.net/manual/en/filter.filters.validate.php 



#### Simple Example

    public function setPostType( $str = false ) {

        return $this->setString( 'property_post_type', $str );
    }




> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --