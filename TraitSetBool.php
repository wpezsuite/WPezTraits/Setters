<?php

namespace WPez\WPezTraits\Setters;

trait TraitSetBool {

    protected function setBool( $str_prop = false, $bool = '', $arr_flags = [] ) {

        if ( property_exists( $this, $str_prop ) ) {

            // http://php.net/manual/en/filter.filters.validate.php
            if ( is_array( $arr_flags ) && filter_var( $bool, FILTER_VALIDATE_BOOLEAN, $arr_flags ) ) {

                $this->$str_prop = $bool;

                return true;

            } else {
                $this->$str_prop = (boolean)$bool;

                return true;
            }

            return false;

        }
    }
}