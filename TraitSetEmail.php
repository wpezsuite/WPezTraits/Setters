<?php
namespace WPez\WPezTraits\Setters;

trait TraitSetEmail {

    protected function setEmail( $str_prop = false, $str_email = false, $arr_flags = [] ) {

        // http://php.net/manual/en/filter.filters.validate.php
        if ( property_exists( $this, $str_prop )
             && is_array($arr_flags)
             && filter_var($str_email, FILTER_VALIDATE_EMAIL, $arr_flags) ) {

            $this->$str_prop = $str_email;

            return true;
        }
        return false;
    }
}