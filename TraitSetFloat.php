<?php

namespace WPez\WPezTraits\Setters;

trait TraitSetFloat {

    protected function setFloat( $str_prop = false, $float = false, $arr_flags =[] ) {

        // http://php.net/manual/en/filter.filters.validate.php
        if ( property_exists( $this, $str_prop )
             && is_array( $arr_flags)
             && filter_var($float, FILTER_VALIDATE_FLOAT, $arr_flags) ) {

            $this->$str_prop = $float;

            return true;
        }

        return false;
    }
}